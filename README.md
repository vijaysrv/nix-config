<div align="center"><img src="https://raw.githubusercontent.com/TheMaxMur/NixOS-Configuration/master/assets/nixos-logo.png" width="300px"></div>
<h1 align="center">Vijay ❄️ NixOS & MacOS Configuration</h1>

<div align="center">

![nixos](https://img.shields.io/badge/NixOS-unstable-blue.svg?style=flat&logo=nixos&logoColor=CAD3F5&colorA=24273A&colorB=8aadf4)
![build](https://img.shields.io/github/actions/workflow/status/vijayakumarravi/nix-config/daily-build.yaml?style=flat&logo=nixos&logoColor=CAD3F5&colorA=24273A&colorB=4bdba4)
![flake check](https://img.shields.io/static/v1?label=Nix%20Flake&message=Check&style=flat&logo=nixos&colorA=24273A&colorB=9173ff&logoColor=CAD3F5)
![license](https://img.shields.io/static/v1.svg?style=flat&label=License&message=Unlicense&colorA=24273A&colorB=91d7e3&logo=unlicense&logoColor=91d7e3&)

</div>

[TODO]

- NixOS Automated Remote Bootstrapping with Secrets -- Ref: [Youtube](https://www.youtube.com/watch?v=4snnV3hdz7g&t=0s) & [Config](https://github.com/EmergentMind/nix-config/blob/dev/scripts/bootstrap-nixos.sh)
